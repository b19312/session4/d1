package com.zuitt.batch193;

public class Child extends Parent{

    //Override
    //The function/method is overridden by replacing the definition of the method in the parent tot the child class.

    @Override //Annotation
    public void speak(){
        System.out.println("I am a mere Child");
    }
}
