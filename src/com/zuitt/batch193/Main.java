package com.zuitt.batch193;

public class Main {

    public static void main(String[] args){

        //Instatiation of object
        //new instance with an empty constructor
        Car myFirstCar = new Car();
        myFirstCar.setName("Civic");
        myFirstCar.setBrand("Honda");
        myFirstCar.setManufactureDate(1998);
        myFirstCar.setOwner("John");

        System.out.println(myFirstCar.getName());

        //new instance with parameterized parameter
        Car mySecondCar = new Car("Charger", "Dodge",   1978, "Vin Diesel");

        //we used the getters from the Car class to retrieve the property of the object
        System.out.println(mySecondCar.getName());
        System.out.println(mySecondCar.getBrand());
        System.out.println(mySecondCar.getManufactureDate());
        System.out.println(mySecondCar.getOwner());

        //methods
        myFirstCar.drive();
        mySecondCar.drive();

        myFirstCar.printDetails();
        mySecondCar.printDetails();

        //Encapsulation
        //we bundled each fields and methods inside a single class and we used access modifiers like public to allow the access from another class

        //why we use encapsulation
        //The fields of a class can be made read-only and write-only
        //a class can have a total control over what is stored in its field.
        //In Java, encapsulation helps us to keep related fields and methods together which make our code cleaner and easy to read.
        //We can achieve data hiding using the access modifier private

        Car anotherCar = new Car();
        anotherCar.setName("Ferrari");
        System.out.println(anotherCar.getName());

        anotherCar.name = "Honda";
        System.out.println(anotherCar.getName());

        //Composition = Ex:A car has a driver
        //allows modelling objects that are made up of other objects. It defines "has a relationship"
        Car newCar = new Car();
        System.out.println("This car is driven by " + newCar.getDriverName());
        System.out.println("A car has a driver " + anotherCar.getDriverName());

        System.out.println("This car HAS a passenger name " + newCar.getPassengerName());
        //everytime we added a new Car object, the Car() always HAS a driver and a passenger.


        //Inheritance = Ex. A car is a vehicle
        //allows modelling objects inherit as a subset of another objects. It defines "IS A relationship"
        //Inheritance can be defined as the process where one class acquires the properties (methods and fields) of another class.

        //"extends" is the keyword used to inherit the properties of a class
        //"super" keyword used for referencing the variable, properties or methods which can be used to another class

        Dog dog1 = new Dog();
        System.out.println(dog1.getBreed()); //Composition

        dog1.setName("Brownie");
        dog1.setColor("Brown");

        System.out.println(dog1.getName());
        System.out.println(dog1.getColor());

        Dog doge = new Dog("Blackie", "Black", "Aspin");
        System.out.println(doge.getBreed());

        //method
        doge.call();
        doge.speak();

        //Interfaces
        //is used to achieve the abstraction
        //In general terms, an interface can be defined as a container that stores the signature of methods to be implemented in the code segment

        Person jane = new Person();
        jane.run();
        jane.sleep();
        jane.morningGreet();
        jane.holidayGreet();

        //POLYMORPHISM
        //the ability of an object to take on many forms
        //There are two main types
        //1. Static or Compile Polymorphism
        //is usually done done by function/method overloading

        StaticPoly print = new StaticPoly();
        System.out.println(print.addition(5, 5));
        System.out.println(print.addition(5, 5, 5));
        System.out.println(print.addition(5.6, 5.8));

        //2. Dynamic Method Dispatch or Runtime polymorphism
        //is usually done by function/method overriding.

        Parent parent1 = new Parent();
        parent1.speak();

        Parent subObject = new Child();//upcasting.
        subObject.speak();//method of sub class or child class is called by Parent reference, this is called "run time polymorphism"
    }

}
